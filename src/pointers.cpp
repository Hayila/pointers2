#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;


  cout<<"x = "<<x<<", y = "<<y<<endl;
  cout<< "Their addresses are: " << &x << " and " <<&y<< " respectively" <<endl; 
  cout <<endl;
 cout<<"a = "<<a<<", b = "<<b<<endl;
  cout<< "Their addresses are: " << &a << " and " <<&b<< " respectively" <<endl; 
 cout <<endl;
  cout<<"c = "<<c<<", d = "<<d<<endl;
  cout<< "Their addresses are: " << &c << " and " <<&d<< " respectively" <<endl; 



}
